Dart Checker takes care of your score, while you are playing darts
alone or with friends. Its the better form of a scoreboard. No manual calculating
is needed anymore. Just put in your hits, like you would tell.

    Features

    * single X01 (301, 501,...), SET/LEG and FREE training mode (variable score), CRICKET
    * 1-4 player (except SET/LEG mode)
    * double and single out
    * checkout suggestions
    * 3x undo in a row of mistouched inputs
    * statistics (about as many matches and player as you like)
    * power saving dark theme or light one
    * language: german / englisch
    * offline - absolutly no internet connection is needed or used, beside app download ;-)